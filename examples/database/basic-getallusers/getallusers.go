/*
Basic way to get all users from the database.
No jwt or authentication added.
I did not use the context package either

database | table | fields
test	 |user   |  id, username
*/

package main

import (
	"database/sql"
	"fmt"

	//have to install driver
	_ "github.com/go-sql-driver/mysql"
)

func main() {

	//database method called to get all users
	GetUsers()

}

/*
the user structure to be used when querying the database
*/
type User struct {
	ID       string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username string `param:"username" query:"username" form:"username" json:"username" xml:"username" validate:"required" min:"4" max:"15" scrub:"name" mod:"trim"`
}

/*
connecting the database to the query
*/
func Conn() *sql.DB {
	                            //db user   conn to db      database used
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
	if err != nil {
		fmt.Println(err)
	}

	//check if it pings
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
	}

	return db
}//end of connect

/*
actual method used to get all users query
*/
func GetUsers() []User {

	data := Conn() //create db instance

	//variables used to store data from the query
	var (
		id       string
		username string
		user     []User //used to store all users
	)
	i := 0 //used to get how many scans

	//get from database
	rows, err := data.Query("select id, username from user")
	if err != nil {
		fmt.Println(err)
	}
	//cycle through the rows to collect all the data
	for rows.Next() {
		err := rows.Scan(&id, &username)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}
		//store into memory
		u := User{ID: id, Username: username}
		user = append(user, u)

	}
	//close everything
	defer rows.Close()
	defer data.Close()
	return user

}
