
/*
Basic way to update a user based on id in the database.
No jwt or authentication added.
I did not use the context package either
database | table | fields
test	 |user   |  id, username

*/

package main

import (
	"database/sql"
	"fmt"
	"log"

	//have to install driver
	_ "github.com/go-sql-driver/mysql"
)

func main() {

	//database method called to get all users
	UpdateUser("2", "jimmy")

}

/*
the user structure to be used when querying the database
*/
type User struct {
	ID       string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username string `param:"username" query:"username" form:"username" json:"username" xml:"username" validate:"required" min:"4" max:"15" scrub:"name" mod:"trim"`
}

/*
connecting the database to the query
*/
func Conn() *sql.DB {
	//db user   conn to db      database used
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
	if err != nil {
		fmt.Println(err)
	}

	//check if it pings
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
	}

	return db
}

/*
actual method used to update user query
*/

func UpdateUser(id string, username string) string {
	//opening database
	data := Conn()
	//prepare statement so that no sql injection
	stmt, e := data.Prepare("update user set username=? where id=?")
	if e != nil {
		log.Fatal(e)
	}
	//execute qeury
	res, e := stmt.Exec(username, id)
	if e != nil {
		log.Fatal(e)
	}
	//used to print rows
	a, e := res.RowsAffected()
	if e != nil {
		log.Fatal(e)
	}

	fmt.Println(a, username)

	return username

}
