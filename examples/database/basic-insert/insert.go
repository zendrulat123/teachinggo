
/*
*This project structure is not for running but this example does work.
*You do need to install "github.com/go-sql-driver/mysql"
*You normally would be getting the data from a form and do jwt and other authen/autho stuff but 
I left it out for learning sake.
*This is just a basic example of an insert without a framework or gorm.
database | table | fields
test	 |user   |  id, username
*/

package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	//have to install driver
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	u := User{ID: "4", Username: "jim"} //you need to create a user object or you can do u := new(User)

	//database method called to create a user
	CreateUser(u)

}

/*
the user structure to be used when querying the database
The extra tags are used for a validate package
*/
type User struct {
	ID       string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username string `param:"username" query:"username" form:"username" json:"username" xml:"username" validate:"required" min:"4" max:"15" scrub:"name" mod:"trim"`
}

/*
connecting the database to the query
can be thrown in another package
*/
func Conn() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/test")
	if err != nil {
		fmt.Println(err)
	}

	//check if it pings
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
	}

	return db
}

/*
actual method used to create an insert query
*/
func CreateUser(u User) {

	data := Conn()  //create db instance
	var exists bool //used for checking

	//create a context query so that you can know if it exists already.
	//if it does then you can stop the context of the request.
	stmts := data.QueryRowContext(context.Background(), "SELECT EXISTS(SELECT 1 FROM user WHERE username=?)", u.Username)
	if err := stmts.Scan(&exists); err != nil {
		log.Fatal(err)
	}
	//prepare the statement to ensure no sql injection
	stmt, err := data.Prepare("INSERT INTO user(id, username) VALUES(?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	//actually make the execution of the query
	res, err := stmt.Exec(u.ID, u.Username)
	if err != nil {
		log.Fatal(err)
	}
	//get last id to double check
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	//get rows affected to double check
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	//print out what you actually did
	log.Printf("ID = %d, affected = %d, username = %d\n", lastId, rowCnt, u.Username)
	defer data.Close()

}
